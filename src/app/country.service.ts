import { HttpClient } from '@angular/common/http';
import { map, Subject } from 'rxjs';
import { CountryModel } from './country.model';
import { Injectable } from '@angular/core';
import { InfocountryModel } from './infocountry.model';

@Injectable()

export class CountryService {
  country: CountryModel[] | null = null;
  change = new Subject<CountryModel[]>()
  countFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getCountry() {
    this.countFetching.next(true);
    this.http.get<{ [id: string]: CountryModel }>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new CountryModel(id, data.name, data.alpha3Code)
        })
      }))
      .subscribe((cont: CountryModel[]) => {
        this.country = cont;
        this.change.next(this.country.slice());
        this.countFetching.next(false);
      }, error => {
        this.countFetching.next(false);
      });
  }

  getCountryOne(id: string) {
    return this.http.get<InfocountryModel>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${id}`)
      .pipe(map(result => {
        console.log(result)
        return new InfocountryModel(id, result.name, result.capital, result.flag, result.timezones, result.population, result.alpha3Code, result.region);
      }))
  }

}
