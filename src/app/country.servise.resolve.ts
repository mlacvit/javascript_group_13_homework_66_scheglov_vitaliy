import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CountryService } from './country.service';
import { InfocountryModel } from './infocountry.model';

@Injectable({
  providedIn: 'root'
})
export class CountryServiceResolve implements Resolve<InfocountryModel> {

  constructor(private service: CountryService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<InfocountryModel> {
    const codeId = route.params['id'];
    return this.service.getCountryOne(codeId);
  }
}
