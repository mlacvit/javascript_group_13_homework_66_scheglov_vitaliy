export class InfocountryModel {
  constructor(
    public id: string,
    public name: string,
    public capital: string,
    public flag: string,
    public timezones: string,
    public population: string,
    public alpha3Code: string,
    public region: string) {
  }
}
