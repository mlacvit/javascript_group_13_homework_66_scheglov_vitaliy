import { Component, OnDestroy, OnInit } from '@angular/core';
import { CountryModel } from './country.model';
import { Subscription } from 'rxjs';
import { CountryService } from './country.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  country: CountryModel[] | null = null;
  countrySubChange!: Subscription;
  countFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(public service: CountryService) {
  }

  ngOnInit(): void {
    this.countFetchingSubscriptions = this.service.countFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    })
    this.countrySubChange = this.service.change.subscribe((count: CountryModel[]) => {
      this.country = count;
    });
    this.service.getCountry();
  }

  goToStart() {
    window.scroll(0, 0);
  }

  ngOnDestroy(): void {
    this.countFetchingSubscriptions.unsubscribe();
    this.countrySubChange.unsubscribe();
  }
}
