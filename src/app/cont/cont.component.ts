import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InfocountryModel } from '../infocountry.model';

@Component({
  selector: 'app-cont',
  templateUrl: './cont.component.html',
  styleUrls: ['./cont.component.css']
})
export class ContComponent implements OnInit {
  country!: InfocountryModel;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.country = <InfocountryModel>data['object'];
    });
  }

  addToFlag() {
    const transformCode = this.country.alpha3Code.toLowerCase();
    return `http://146.185.154.90:8080/restcountries/data/${transformCode}.svg`;
  }

}
