import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContComponent } from './cont/cont.component';
import { CountryServiceResolve } from './country.servise.resolve';

const routes: Routes = [
  {
    path: ':id',
    component: ContComponent,
    resolve: {
      object: CountryServiceResolve
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
