export class CountryModel {
  constructor(public id: string, public name: string, public alpha3Code: string) {
  }
}
