import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContComponent } from './cont/cont.component';
import { HttpClientModule } from '@angular/common/http';
import { CountryService } from './country.service';

@NgModule({
  declarations: [
    AppComponent,
    ContComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
